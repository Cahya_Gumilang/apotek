'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('firstname', 50)
      table.string('lastname', 50)
      table.string('email', 50).notNullable()
      table.string('password', 100).notNullable()
      table.string('gender', 25)
      table.date('dob')
      table.string('phone', 15)
      table.integer('point').defaultTo(0)
      table.string('photo')
      table.string('token')
      table.string('forgot_token').defaultTo(null)
      table.string('remember_token').defaultTo(null)
      table.boolean('status').defaultTo(false)
      table.boolean('is_deleted').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
