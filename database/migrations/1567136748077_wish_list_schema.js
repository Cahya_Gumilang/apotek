'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WishListSchema extends Schema {
  up () {
    this.create('wish_lists', (table) => {
      table.increments()
      table.integer('product_id').unsigned()
      table.integer('user_id').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('wish_lists')
  }
}

module.exports = WishListSchema
