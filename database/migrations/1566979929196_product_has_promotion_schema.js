'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductHasPromotionSchema extends Schema {
  up () {
    this.create('product_has_promotions', (table) => {
      table.increments()
      table.integer('product_id').unsigned()
      table.integer('promotion_id').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_has_promotions')
  }
}

module.exports = ProductHasPromotionSchema
