'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSubCategorySchema extends Schema {
  up () {
    this.create('product_sub_categories', (table) => {
      table.increments()
      table.integer('product_category_id').unsigned()
      table.string('name', 50)
      table.boolean('is_deleted').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('product_sub_categories')
  }
}

module.exports = ProductSubCategorySchema
