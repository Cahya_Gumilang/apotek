'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdminFunctionSchema extends Schema {
  up () {
    this.create('admin_functions', (table) => {
      table.increments()
      table.integer('admin_id').unsigned()
      table.boolean('manage_admins').defaultTo(false)
      table.boolean('manage_users').defaultTo(false)
      table.boolean('manage_products').defaultTo(false)
      table.boolean('manage_transactions').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('admin_functions')
  }
}

module.exports = AdminFunctionSchema
