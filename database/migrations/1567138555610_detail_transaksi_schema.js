'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailTransaksiSchema extends Schema {
  up () {
    this.create('detail_transaksis', (table) => {
      table.increments()
      table.integer('transaksi_id').unsigned()
      table.integer('product_id').unsigned()
      table.integer('quantity')
      table.integer('price')
      table.integer('discount')
      table.timestamps()
    })
  }

  down () {
    this.drop('detail_transaksis')
  }
}

module.exports = DetailTransaksiSchema
