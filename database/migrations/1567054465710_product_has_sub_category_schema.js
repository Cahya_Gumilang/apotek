'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductHasSubCategorySchema extends Schema {
  up () {
    this.create('product_has_sub_categories', (table) => {
      table.increments()
      table.integer('product_id').unsigned()
      table.integer('product_sub_category_id').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_has_sub_categories')
  }
}

module.exports = ProductHasSubCategorySchema
