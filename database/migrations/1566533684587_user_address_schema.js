'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserAddressSchema extends Schema {
  up () {
    this.create('user_addresses', (table) => {
      table.increments()
      table.integer('user_id').unsigned()
      table.string('nama_penerima', 50)
      table.string('phone', 15)
      table.string('nama_alamat', 50)
      table.string('provinsi', 25)
      table.string('kota', 25)
      table.string('kecamatan', 25)
      table.text('alamat')
      table.string('kode_pos', 10)
      table.boolean('alamat_utama').defaultTo(false)
      table.boolean('is_deleted').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('user_addresses')
  }
}

module.exports = UserAddressSchema
