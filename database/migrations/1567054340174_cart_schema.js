'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CartSchema extends Schema {
  up () {
    this.create('carts', (table) => {
      table.increments()
      table.integer('product_id').unsigned()
      table.integer('user_id').unsigned()
      table.integer('qty').defaultTo(1)
      table.boolean('is_checkout').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('carts')
  }
}

module.exports = CartSchema
