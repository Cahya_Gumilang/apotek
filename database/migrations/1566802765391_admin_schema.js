'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdminSchema extends Schema {
  up () {
    this.create('admins', (table) => {
      table.increments()
      table.string('firstname', 25)
      table.string('lastname', 25)
      table.string('email', 50).notNullable()
      table.string('password', 100).notNullable()
      table.string('gender', 25)
      table.date('dob')
      table.string('phone', 15)
      table.string('photo').notNullable()
      table.boolean('manage_admins').defaultTo(false)
      table.boolean('manage_users').defaultTo(false)
      table.boolean('manage_products').defaultTo(false)
      table.boolean('manage_transactions').defaultTo(false)
      table.boolean('status').defaultTo(false)
      table.boolean('is_deleted').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('admins')
  }
}

module.exports = AdminSchema
