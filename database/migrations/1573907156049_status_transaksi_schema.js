'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const Database = use('Database')
class StatusTransaksiSchema extends Schema {
  up () {
    this.create('status_transaksis', (table) => {
      table.increments()
      table.integer('transaksi_id').unsigned()
      table.timestamp('diproses').defaultTo(Database.fn.now())
      table.timestamp('dikirim').nullable(true).defaultTo(null)
      table.timestamp('diterima').nullable(true).defaultTo(null)
      table.string('current_status', 100).defaultTo('diproses')
      table.timestamps()
    })
  }

  down () {
    this.drop('status_transaksis')
  }
}

module.exports = StatusTransaksiSchema
