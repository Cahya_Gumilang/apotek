'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const Database = use('Database')
class TransaksiSchema extends Schema {
  up () {
    this.create('transaksis', (table) => {
      table.increments()
      table.string('invoice').unique().notNullable()
      table.integer('user_id').unsigned()
      table.integer('user_address_id').unsigned()
      table.integer('courier_id').unsigned()
      table.integer('payment_method_id').unsigned()
      table.integer('point')
      table.timestamps('tgl_transaksi').defaultTo(Database.fn.now())
      table.string('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('transaksis')
  }
}

module.exports = TransaksiSchema
