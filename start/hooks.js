const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
  const Env = use('Env')  
  const View = use('View')
  View.global('APP_URL', () => Env.get('APP_URL'))
})