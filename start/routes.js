'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const CloudinaryService = use('App/Services/CloudinaryService');
    // APIs
    Route.group(()=>{
        Route.get('/list','ProductCategoryController.listCategoryHasSub')
        Route.resource('/', 'ProductCategoryController')
    }).prefix('/category').middleware(['auth'])

    Route.group(()=>{       
        Route.resource('/', 'ProductSubCategoryController')
    }).prefix('/subcategory').middleware(['auth'])

    Route.group(()=>{
        Route.get('/','CartController.index')
        Route.get('/getbill', 'CartController.getCheckoutBills')
        Route.post('/','CartController.store')
        Route.delete('/:id','CartController.destroy')
        Route.put('/:id', 'CartController.updateQty')
        Route.put('/setcheck/:id', 'CartController.setCheckOut')
        Route.get('/relatedproduct', 'CartController.getRelatedProduct')
    }).prefix('/cart').middleware(['auth'])

    Route.group(()=>{
        Route.get('/', 'WishListController.index')
        Route.post('/', 'WishListController.store')
        Route.post('/tocart', 'WishListController.insertToCart')
        Route.post('/alltocart', 'WishListController.insertAllToCart')
        Route.delete('/:id', 'WishListController.destroy')
        Route.get('/relatedproduct', 'WishListController.getRelatedProduct')
    }).prefix('/wishlist').middleware(['auth'])

    Route.group(()=>{
        Route.get('/list', 'ProductController.listProduct')
        Route.get('/detail/:id', 'ProductController.detail')
        Route.get('/list/category/:id', 'ProductController.listByCategory')
        Route.get('/list/category/all/:id', 'ProductController.listByCategoryAll')
        Route.get('/list/subcategory/:id', 'ProductController.listBySubCategory')
        Route.get('/search','ProductController.search')
        Route.resource('/', 'ProductController')
    }).prefix('/product').middleware(['auth'])
    
    Route.group(()=>{
        Route.post('/changepassword', 'AuthController.changePassword')
    }).middleware(['auth'])

    Route.group(()=>{
        Route.resource('/','UserController')
        Route.post('/update/:id', 'UserController.update')
    }).prefix('/user').middleware(['auth'])

    Route.group(()=>{
        Route.get('/login/google', 'LayoutController.login')
        Route.get('/login_social/:provider', 'AuthController.doSocialLogin')
        Route.get('/show','AuthController.show').middleware(['auth'])
        Route.get('/verification/:token', 'AuthController.verifyEmail')
        Route.get('/checkemail/:id', 'AuthController.checkEmailStatus')
        Route.post('/login', 'AuthController.doLogin')
        Route.post('/register', 'AuthController.register')
        Route.post('/forgotpassword', 'AuthController.sendEmailForgotPassword')
        Route.get('/forgotpassword/view/:token', 'AuthController.renderFormForgotPassword')
        Route.post('/forgotpassword/reset', 'AuthController.resetPassword').as('reset')
    }).prefix('/auth')

    Route.group(()=>{
        Route.get('/getprovinces', 'UserAddressController.getProvinces')
        Route.get('/getcities', 'UserAddressController.getCities')
        Route.get('/getdistricts', 'UserAddressController.getDistricts')
        Route.get('/', 'UserAddressController.index')
        Route.get('/list', 'UserAddressController.getList')
        Route.get('/:id', 'UserAddressController.showDetail')
        Route.post('/','UserAddressController.store')
        Route.put('/:id', 'UserAddressController.update')
        Route.put('/setmain/:id', 'UserAddressController.setAlamatUtama')
        Route.delete('/:id', 'UserAddressController.destroy')
    }).prefix('/address').middleware(['auth'])


    Route.group(()=>{
        Route.resource('/', 'CourierController')
    }).prefix('/courier').middleware(['auth'])

    Route.group(()=>{
        Route.resource('/', 'PaymentMethodController')
    }).prefix('/paymentmethod').middleware(['auth'])

    Route.group(()=>{
        Route.get('/getbills', 'TransaksiController.getBills')
        Route.resource('/', 'TransaksiController')
    }).prefix('/transaksi').middleware(['auth'])

    // CMS
    Route.group(()=>{
        Route.get('/dashboard', 'DashboardController.index').as('index')
        Route.get('/user', 'UserController.index').as('user')
        Route.put('/admin/manage/:id', 'AdminController.updateManage')
        Route.get('/product/sub/delete', 'ProductController.destroySub')
        Route.get('/product/image/delete', 'ProductController.destroyImage')
        Route.get('/category/sub/create/:id', 'CategoryController.createSub')
        Route.post('/category/sub/:id', 'CategoryController.storeSub')
        Route.delete('/category/sub/:id', 'CategoryController.destroySub')
        Route.put('/transaksi/confirm/:id', 'TransaksiController.sendConfirm')
        Route.resource('/user', 'UserController')
        Route.resource('/admin', 'AdminController')
        Route.resource('/product', 'ProductController')
        Route.resource('/transaction', 'TransaksiController')
        Route.resource('/courier', 'CourierController')
        Route.resource('/payment', 'PaymentMethodController')
        Route.resource('/category', 'CategoryController')
        Route.get('/logout', 'AuthController.doLogout').as('logout')       
    }).prefix('/admin').middleware(['authAdmin']).namespace('Admin')

    Route.group(()=>{
        Route.get('/', ({response})=>{response.redirect('/admin/login')})
        Route.get('/login', 'LayoutController.login')
        Route.post('/login', 'AuthController.doLogin').as('login')
    }).prefix('/admin').namespace('Admin')
    
    Route.group(()=>{
        Route.get('/user', 'UserController.loadDataUser')
        Route.get('/dashboard/getdata', 'DashboardController.index')
    }).prefix('/api/v1/admin').middleware(['authAdmin']).namespace('Admin')

    
    //TEST
    Route.get('/test', 'TransaksiController.index').namespace('Admin')
    
