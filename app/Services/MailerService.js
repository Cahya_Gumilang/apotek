'use strict'

const nodemailer = require('nodemailer')
const Env = use('Env')
const senderEmail = Env.get('APP_EMAIL', 'email.untuk.ta.ya@gmail.com')
const senderPassword = Env.get('APP_EMAIL_PASSWORD', 'qwerty961')
const view = use('View')
const sgMail = require('@sendgrid/mail');
const sgKey = Env.get('SENDGRID_API_KEY')

class MailerService{

    static sendVerificationEmail(email, user){
        try{
            sgMail.setApiKey(sgKey);
        let data = {
            firstname : user.firstname,
            lastname : user.lastname,
            link : user.link
        }

        const msg = {
            to: email,
            from: 'cahyagumilang0@gmail.com',
            subject: 'Email Registration',
            html: view.render('mail.registration2', {data})
          
          };

          sgMail.send(msg);
        }catch(err){
            console.log(err.message)
        }
    }

    static sendForgotEmail(email, user){
        sgMail.setApiKey(sgKey);
        let data = {
            firstname : user.firstname,
            lastname : user.lastname,
            link : user.link
        }

        const msg = {

            to: email,
          
            from: 'cahyagumilang0@gmail.com',
          
            subject: 'Forgot Password',

            html: view.render('mail.forgot_password', {data})
          
          }
          
          sgMail.send(msg)
    }


}

module.exports = MailerService