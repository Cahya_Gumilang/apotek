'use strict'
const Product = use('App/Models/Product')
const Cart = use('App/Models/Cart')
const UserAddress = use('App/Models/UserAddress')
const Env = use('Env')
const shipping = use('shipping-indonesia')

class TransaksiService{
    static async getCheckoutBills(user_id){
        shipping.init(Env.get('SHIPPING_KEY'))
        const query = Cart.query()
            .where('user_id',user_id)
            .where('is_checkout', true)
            .with('product', builder=>{
                builder.select(['id','name','thumbnail','price','stok','discount'])
            })
        const data = await query.fetch()
        const count = await query.count('* as total')
        var total_price = 0
        var discount = 0
        var weight = 0
        if(data.toJSON()[0]){
            data.toJSON().forEach(item => {
                total_price += (item.product.price * item.qty)
                discount += (item.product.price*item.product.discount/100)*item.qty
                weight += 200*item.qty
            })
        }
        const cost = 110000
        return {
            jumlah_pesanan : count[0].total,
            total_harga : total_price,
            potongan : discount,
            ongkir : cost,
            total_bayar : total_price - discount + cost
        }
    }
}

module.exports = TransaksiService