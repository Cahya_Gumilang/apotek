'use strict'

const randomstring = require('randomstring')

class UtilityService{
    static generateRandomToken(){
        return randomstring.generate({
            length : '5',
            capitalization : 'uppercase',
            charset : 'alphanumeric'
        })
    }
}

module.exports = UtilityService