'use strict'

class CRUDService{

    /**
     * 
     * @param {*} model 
     * @param {*} id 
     * @param {*} permanent 
     */
    static async destroy (model, id, permanent = false){
        const Model = use(model)
        let data = await Model.find(id)
        if(!data) throw 'Data Not Found'
        if(permanent){
            await data.delete()
        }
        else{
            data.merge({'is_deleted': true})
            await data.save()
        }
        
        return data
    }

    /**
     * 
     * @param {*} model 
     * @param {*} id 
     * @param {*} data 
     */
    static async updateAttribute(model, id, data = {}){
        const Model = use(model)
        const dataModel = await Model.find(id)
        if(!dataModel) return false
        dataModel.merge(data)
        await dataModel.save()
        return dataModel
    }

    /**
     * 
     * @param {*} model 
     * @param {*} data 
     */
    static async store(model, data = {}){
        const Model = use(model)
        let dataModel = new Model()
        dataModel.fill(data)
        await dataModel.save()
        return dataModel
    }

    static async getSingleRecord(model, whereQuery={}){
        const Model = use(model)
        let dataModel = await Model.query().where(whereQuery).fetch()

        dataModel = dataModel.toJSON()
        return dataModel[0]
    }

    /**
     * 
     * @param {*} model 
     * @param {*} id 
     */

    static async getRecordBy(model, by, byData){
        const Model = use(model)
        let dataModel = await Model.findBy(by, byData)

        return dataModel
    }

    static async getRecordById(model, id){
        const Model = use(model)
        let dataModel = await Model.find(id)
        if(!dataModel) throw 'Data Not Found'
        return dataModel
    }

    /**
     * 
     * @param {*} model 
     * @param {*} id 
     * @param {*} withRelation 
     * @param {*} filter 
     * @param {*} key 
     * @param {*} whereQuery 
     */
    static async getSingleRecordWithRelation(model, id, withRelation = [], filter = [], key, whereQuery = {}){
        const Model = use(model)
        if(!key) key = ''
        if(!filter) filter = []
        if(!whereQuery) whereQuery = {}
        
        let query = Model.query()
            .where('id', id)
            .where('is_deleted', false)
            .where(whereQuery)
            .where((table)=>{
                filter.forEach(index=>{
                    table.orWhere(index,'like','%'+key+'%')
                })
            })
        withRelation.forEach(item=>{
            query = query.with(item)
        })
        let dataModel = await query.fetch()
        dataModel = dataModel.toJSON()
        console.log(dataModel[0])
        if(!dataModel[0]) return false
        return dataModel[0]
    }

    /**
     * 
     * @param {*} model 
     * @param {*} params 
     * @param {*} ids 
     */
    static async getRecordsByIds(model, params, ids = []){
        const Model = use(model)
        let page = params.page ? params.page : 1
        let limit = params.limit ? params.limit : 20

        const data = Model.query().whereIn('id', ids).paginate(page, limit)
        return data
    }
    
    /**
     * 
     * @param {*} model : {'App/Models/ModelName'}
     * @param {*} params : {page, limit, key}
     * @param {*} whereQuery
     * @param {*} withRelation 
     * @param {*} filter 
     * @param {*} selectQuery 
     */
    static async getRecordWithRelation(model, params, whereQuery = {}, withRelation = [], filter = [], selectQuery = '*'){
        const Model = use(model)
        let page = params.page ? params.page : 1
        let limit = params.limit ? params.limit : 20
        let key = params.key ? params.key : ''
        
        selectQuery = selectQuery.split(',')
        
        let query = Model.query()
            .select(selectQuery)
            .where('is_deleted', false)
            .where(whereQuery)
            .where((table)=>{
                filter.forEach(index=>{
                    table.orWhere(index,'like','%'+key+'%')
                })
            })
        
        withRelation.forEach(item=>{
            query = query.with(item)
        })
        
        let data = await query.paginate(page, limit)
        data = data.toJSON()
        console.log(data.total)
        if(data.total<1) return false
        return data
    }

    
}

module.exports = CRUDService