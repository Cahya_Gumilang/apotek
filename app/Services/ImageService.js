'use strict'

const CloudinaryService = use('App/Services/CloudinaryService')

class ImageService{
    static saveImage(imageFile, folder, cb = (error, reult)=>{}){
        return CloudinaryService.v2.uploader.upload(imageFile.tmpPath, {folder}, cb);
    }

    static destroyImage(public_id, cb = (error, reult)=>{}){
        CloudinaryService.uploader.destroy(public_id, cb)
    }

    static getPublicId(url, folder){
        return url.substring(url.lastIndexOf(folder),url.lastIndexOf('.'))
    }
}

module.exports = ImageService