'use strict'


const Product = use('App/Models/Product')
const Cart = use('App/Models/Cart')
class CartService{
    static async getCheckoutBills(user_id, ongkir = false){
        const query = Cart.query()
            .where('user_id',user_id)
            .where('is_checkout', true)
            .with('product', builder=>{
                builder.select(['id','name','thumbnail','price','stok','discount'])
            })
        const data = await query.fetch()
        const count = await query.count('* as total')
        var total_price = 0
        var discount = 0
        if(data.toJSON()[0]){
            data.toJSON().forEach(item => {
                total_price += (item.product.price * item.qty)
                discount += (item.product.price*item.product.discount/100)*item.qty
            })
        }
        let res = {
            jumlah_pesanan : count[0].total,
            total_harga : total_price,
            potongan : discount,
            total_bayar : total_price - discount
        }

        if(ongkir) {
            res.ongkir = 110000
            res.total_bayar += res.ongkir
        }
        
        return res
    }

}

module.exports = CartService