'use strict'

const jwt = require('jsonwebtoken')
const Env = use('Env')

class JWTService{
    static async generateToken(payload){
        return new Promise((resolve, reject) => {
            jwt.sign(JSON.stringify(payload), Env.get('JWT_SECRET'), (err, token) => {
                if (err) {
                    const payload2 = this.decodeJWT(token);

                    delete payload2.iat;

                    delete payload2.exp;

                    delete payload2.nbf;

                    delete payload2.jti; 

                    return jwt.sign(payload2, Env.get('JWT_SECRET'));
                } else {
                    return resolve(token)
                }
            })
        })
    }

    static decodeJWT(token) {
        try {
            return jwt.verify(token, Env.get('JWT_SECRET'));
        } catch (error) {
            return null
        }    
    }
}

module.exports = JWTService