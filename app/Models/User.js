'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')
const moment = require('moment')
class User extends Model {
  static boot () {
    super.boot()
    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
      if(!userInstance.photo){
        if(userInstance.gender == 'Pria'){
          userInstance.photo = Env.get('CLOUDINARY_MALE_DEFAULT')
        }
        else{
          userInstance.photo = Env.get('CLOUDINARY_FEMALE_DEFAULT')
        }
      }
    })
  
    
    
  }
  
  static get hidden () {
    return ['password','created_at','updated_at']
  }

  static get dates () {
    return super.dates.concat(['dob'])
  }

  static castDates (field, value) {
    if (field === 'dob') {
      return value.format('YYYY-MM-DD')
    }
    return super.formatDates(field, value)
  }
  
  static scopeHasActive(query){
    return query.where('status', true)
  }

  carts () {
    return this.hasMany('App/Models/Cart')
  }

  wish_lists () {
    return this.hasMany('App/Models/WishList')
  }

  user_addresses () {
    return this.hasMany('App/Models/UserAddress')
  }

  transaksi () {
    return this.hasMany('App/Models/Transaksi')
  }

}

module.exports = User
