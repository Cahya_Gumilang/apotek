'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Transaksi extends Model {

    static get dates () {
        return super.dates.concat(['tgl_transaksi'])
    }

    detail_transaksi() {
        return this.hasMany('App/Models/DetailTransaksi')
    }

    user() {
        return this.belongsTo('App/Models/User')
    }

    user_address() {
        return this.belongsTo('App/Models/UserAddress')
    }

    courier() {
        return this.belongsTo('App/Models/Courier')
    }

    payment_method() {
        return this.belongsTo('App/Models/PaymentMethod')
    }

    status () {
        return this.hasOne('App/Models/StatusTransaksi')
    }

    admin () {
        return this.belongsTo('App/Models/Admin')
    }
}

module.exports = Transaksi
