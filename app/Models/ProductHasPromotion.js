'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductHasPromotion extends Model {
}

module.exports = ProductHasPromotion
