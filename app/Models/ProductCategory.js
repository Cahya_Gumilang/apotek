'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductCategory extends Model {
    sub_categories () {
        return this.hasMany('App/Models/ProductSubCategory')
    }
}

module.exports = ProductCategory
