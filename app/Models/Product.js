'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
    product_images () {
        return this.hasMany('App/Models/ProductImage')
    }
    sub_categories () {
        return this.belongsToMany('App/Models/ProductSubCategory', 'product_id', 'product_sub_category_id').pivotTable('product_has_sub_categories').withPivot(['id'])
    }
    promotions () {
        return this.belongsToMany('App/Models/Promotion', 'product_id', 'promotion_id').pivotTable('product_has_promotions')
    }
}

module.exports = Product
