'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')
const Env = use('Env')
class Admin extends Model {
    static boot () {
        super.boot()
        /**
         * A hook to hash the user password before saving
         * it to the database.
         */
        this.addHook('beforeSave', async (userInstance) => {
          if (userInstance.dirty.password) {
            userInstance.password = await Hash.make(userInstance.password)
          }
          if(!userInstance.photo){
            if(userInstance.gender == 'Laki Laki'){
              userInstance.photo = Env.get('CLOUDINARY_MALE_DEFAULT')
            }
            else{
              userInstance.photo = Env.get('CLOUDINARY_FEMALE_DEFAULT')
            }
          }
        })
      }    
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  static get dates () {
    return super.dates.concat(['dob'])
  }

  static castDates (field, value) {
    if (field === 'dob') {
      return value.format('YYYY-MM-DD')
    }
    return super.formatDates(field, value)
  }

  functions (){
    return this.hasOne('App/Models/AdminFunction')
  }
}

module.exports = Admin
