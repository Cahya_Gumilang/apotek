'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StatusTransaksi extends Model {
    static get dates () {
        return super.dates.concat(['diproses','dikirim','diterima'])
    }
}

module.exports = StatusTransaksi
