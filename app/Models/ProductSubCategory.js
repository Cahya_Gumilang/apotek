'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductSubCategory extends Model {
    category () {
        return this.belongsTo('App/Models/ProductCategory')
    }

    static get hidden () {
        return ['product_category_id']
    }
}

module.exports = ProductSubCategory
