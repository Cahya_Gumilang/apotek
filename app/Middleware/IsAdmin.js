'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const JWTService = use('App/Services/JWTService')
const view = use('View')
const Admin = use('App/Models/Admin')

class Auth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, response}, next) {
    const is_admin = await Admin.findBy('user_id', request.decodeToken.id)
    if(is_admin){
        await next()
    }
    else{
        
    }
  }
}

module.exports = Auth
