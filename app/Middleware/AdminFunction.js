'use strict'
const Admin = use('App/Models/Admin')
class AdminFunction{
    async handle({request, response, auth},next){
        const admin = await Admin.query().where('id',auth.user.id).with('functions').first()
        auth.user.function = {
            manage_admins : admin.toJSON().functions.manage_admins,
            manage_users : admin.toJSON().functions.manage_users,
            manage_products : admin.toJSON().functions.manage_products,
            manage_transactions : admin.toJSON().functions.manage_transactions,
        }
        await next()
    }
}

module.exports = AdminFunction