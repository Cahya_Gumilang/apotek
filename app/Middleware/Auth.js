'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const JWTService = use('App/Services/JWTService')
const User = use('App/Models/User')
const view = use('View')
class Auth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, response}, next) {
    // call next to advance the request
    const headers = request.headers();
    var token = headers.accesstoken

    var decodeToken = JWTService.decodeJWT(token);
    if (decodeToken && await User.find(decodeToken.id)) {
      request.decodeToken = decodeToken;
      
      await next()
    } else {
        response.status(401).send({
          code: 401,
          message: "Unauthorized"
        })
      
    }
  }
}

module.exports = Auth
