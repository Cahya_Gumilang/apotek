'use strict'

class AuthAdmin{
    async handle({request, response, auth},next){
        try {
            await auth.check()
            var decodeToken = await auth.getUser()
            request.decodeToken = decodeToken
          } catch (error) {
            return response.redirect('/admin')
          }
          
          await next()
    }
}

module.exports = AuthAdmin