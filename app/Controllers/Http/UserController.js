'use strict'

const User = use('App/Models/User') 
const Admin = use('App/Models/Admin')
const CRUDService = use('App/Services/CRUDService')
const md5 = require('md5')
const MailerService = use('App/Services/MailerService')
const Env = use('Env')
const ImageService = use('App/Services/ImageService');

class UserController {
    async index({request, response}) {
        const params = request.all()
        let page = params.page ? params.page : 1
        let limit = params.limit ? params.limit : 10
        const offset = (page-1)*limit

        const data = await User.query()
            .where('is_deleted', false)
            .paginate(page, limit)
        
        let json = data.toJSON()
        if(!json.data[0]) return response.ok('Data Not Found')
        json.content = json.data
        delete json.data

        return response.ok(json, 'list of user') 
    }

    async showAllDetails({request, response}){
        const params = request.all()
        let page = params.page ? params.page : 1
        let limit = params.limit ? params.limit : 10
        let select = params.select ? params.select : '*'
        let key = params.key ? params.key : '*'
        const offset = (page-1)*limit

        const data = await CRUDService.getRecordWithRelation('')
        
        let json = data.toJSON()
        if(!json.data[0]) return response.ok('Data Not Found')
        json.content = json.data
        delete json.data

        return response.ok(json, 'list of user') 
    }

    async showUserCarts({request, response}){

    }

    async showUserWishLists({request, response}){

    }

    async showUserDetails({request, response}){

    }

    async show({request, response, params}) {
        try{
            const data = await CRUDService.getRecordById('App/Models/User',params.id)
            return response.ok(data)
        }catch(err){
            return response.ok(err.message)
        } 
    }

    async store({request, response}) {
        try{
            const data = request.all()
            console.log(data)
            const photo = request.file('photo', {
                types: ['image'],
                size: '10mb'
            })
            if(photo){
                const cloudinaryResponse = await ImageService.saveImage(photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                data.photo = cloudinaryResponse.secure_url
            }
            const user = new User()
            user.fill(data)
            user.status = true
            await user.save() 
            return response.ok(user, 'User was created')
        }catch(err){
            return response.ok(err.message)
        }
    }

    async update({request, response, params}) {
        try{
            const data = request.all()
            const photo = request.file('photo', {
                types: ['image'],
                size: '10mb'
            })
            let user = await User.find(params.id)
            if(!user) throw 'User Not Found'

            if(photo){
                const cloudinaryResponse = await ImageService.saveImage(photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                data.photo = cloudinaryResponse.secure_url
                const oldPublicId = ImageService.getPublicId(user.photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                const publicIdDefault = [ImageService.getPublicId(Env.get('CLOUDINARY_MALE_DEFAULT')), ImageService.getPublicId(Env.get('CLOUDINARY_FEMALE_DEFAULT')) ]
                if(oldPublicId!=publicIdDefault[0] && oldPublicId!=publicIdDefault[1])
                await ImageService.destroyImage(oldPublicId)
            }

            user.merge(data)
            await user.save()

            return response.ok(user, 'Update Success')
        }catch(err){
            return response.ok(err.message)
        }
    }

    async destroy({request, response, params}) {   
        try{
            let user = await CRUDService.destroy('App/Models/User', params.id)

            if((user.photo != Env.get('CLOUDINARY_MALE_DEFAULT')) && (user.photo != Env.get('CLOUDINARY_FEMALE_DEFAULT'))){
                const publicId = ImageService.getPublicId(user.photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                await ImageService.destroyImage(publicId)
            }

            return response.ok(null, 'Delete Success')
        }catch(err){
            return response.ok(err.message)
        }
    }

}

module.exports = UserController
