'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with transaksis
 */
const Transaksi = use('App/Models/Transaksi')
const DetailTransaksi = use('App/Models/DetailTransaksi')
const StatusTransaksi = use('App/Models/StatusTransaksi')
const Cart = use('App/Models/Cart')
const Product = use('App/Models/Product')
const User = use('App/Models/User')
const TransaksiService = use('App/Services/TransaksiService')
const moment = require('moment')
const Courier = use('App/Models/Courier')
class TransaksiController {
  /**
   * Show a list of all transaksis.
   * GET transaksis
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    try{
    const data = await Transaksi.query().where('user_id',request.decodeToken.id)
      .with('detail_transaksi.product')
      .with('status')
      .fetch()
    

    const res = data.toJSON().map(item=>{
      let item2 = {
        id : item.id,
        invoice : item.invoice,
        tgl_transaksi : item.tgl_transaksi,
        status : item.status.current_status,
        point : item.point,
        ongkir : 11000,
        total_bayar : 11000,
        total_product : 0
      }
      item.detail_transaksi.forEach(item3=>{
        item2.total_bayar +=  ((item3.price - (item3.price*item3.discount/100))*item3.qty)
        item2.total_product++
      })

      let detail = item.detail_transaksi[0]
      console.log(detail)
      item2.product = {
        id : detail.product.id,
        name : detail.product.name,
        thumbnail : detail.product.thumbnail,
        price : "Rp"+(detail.price - (detail.price*detail.discount/100)),
        qty : detail.qty+" Produk",
        total_harga : "Rp" +(detail.price - (detail.price*detail.discount/100))*detail.qty
      }

      return item2
    })

    return response.ok(res)
  }catch(err){
    return response.ok(err.message)
  }
  }

  /**
   * Render a form to be used for creating a new transaksi.
   * GET transaksis/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new transaksi.
   * POST transaksis
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    try{
      const data = request.all()
      data.user_id = request.decodeToken.id
      const transaksi = await Transaksi.query()
        .where('invoice','like','INV/'+new Date().getFullYear()+'%')
        .orderBy('created_at', 'DESC')
        .first()
      
      if(transaksi) data.invoice = "INV/"+(Number.parseInt(transaksi.invoice.substring(transaksi.invoice.indexOf('/')+1)) + 1).toString()
      else data.invoice = "INV/"+new Date().getFullYear()+"00001"
      const result = new Transaksi()
      result.merge(data)
      await result.save()

      const query_cart = Cart.query().where('is_checkout',true)
        .where('user_id',data.user_id)
        .with('product', builder=>{
          builder.select(['id','name','thumbnail','price','stok','discount'])
        })
      
      const cart_data = await query_cart.fetch()
      let point = 0
      const transaksi_detail = cart_data.toJSON().map(item=>{
        let item2 = {}
        item2.transaksi_id = result.id
        item2.product_id = item.product.id
        item2.price = item.product.price
        item2.discount = item.product.discount
        item2.qty = item.qty
        point += Math.floor((item.product.price-(item.product.price*item.product.discount/100))/1000)
        return item2
      })
      data.point = point
      result.point = point
      await result.save()

      await DetailTransaksi.createMany(transaksi_detail)
      await StatusTransaksi.create({transaksi_id : result.id})
      await query_cart.delete()
      for(let item of cart_data.toJSON()){
        console.log(item)
        await Product.query().where('id',item.product.id).update({stok : item.product.stok - item.qty})
      }

      const user = await User.find(data.user_id)
      user.point += point
      await user.save()
      
      return response.ok(null)
    }catch(err){
      return response.ok(err.message)
    }
  }

  /**
   * Display a single transaksi.
   * GET transaksis/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let data = await Transaksi.query().where('user_id',request.decodeToken.id)
      .where('id',params.id)
      .with('detail_transaksi.product')
      .with('courier')
      .with('payment_method')
      .with('status')
      .with('user_address')
      .first()

    data = data.toJSON()
      let res = {
        id : data.id,
        invoice : data.invoice,
        tgl_transaksi : data.tgl_transaksi,
        status : data.status.current_status,
        point : data.point,
        courier: data.courier.name,
        ongkir : 11000,
        payment : data.payment_method.name,
        total_bayar : 11000,
        total_product : 0,
        alamat : data.user_address.alamat + ', ' + data.user_address.kecamatan + ', ' + data.user_address.kota + ', ' + data.user_address.provinsi + ', ' + data.user_address.kode_pos,
        no_telp : data.user_address.phone
      }

      if(res.status=="Diproses"){
        res.estimasi = "-"
      }

      else{
        res.estimasi = moment(data.tgl_transaksi).add(3,'days').format('YYYY-MM-DD')
      }

      data.detail_transaksi.forEach(item2=>{
        res.total_bayar +=  (item2.price - (item2.price*item2.discount/100))*item2.qty
        res.total_product++
      })

      res.product = data.detail_transaksi.map(detail=>{
        let detail2 = {
          id : detail.product.id,
          name : detail.product.name,
          thumbnail : detail.product.thumbnail,
          price : "Rp"+(detail.price - (detail.price*detail.discount/100)),
          qty : detail.qty+" Produk",
          total_harga : "Rp"+(detail.price - (detail.price*detail.discount/100))*detail.qty
        }

        return detail2
      })


    return response.ok(res)
  }

  /**
   * Render a form to update an existing transaksi.
   * GET transaksis/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update transaksi details.
   * PUT or PATCH transaksis/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a transaksi with id.
   * DELETE transaksis/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async getBills ({ params, request, response }) {
    const data = request.all()
    data.courier = data.courier ? data.courier : 0
    const res = await TransaksiService.getCheckoutBills(request.decodeToken.id, data.user_address_id, data.courier)

    return response.ok(res)
  }
}

module.exports = TransaksiController
