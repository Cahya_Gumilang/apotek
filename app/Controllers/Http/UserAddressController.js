'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with useraddresses
 */
// AIzaSyA0uh0QFdHNEma4M3UGukhHHj3ogjfezF8
// AIzaSyDMzoCDxB5o2oC88_H5-QJAmbc36W-9Ztk
const NodeGeocoder = require('node-geocoder')
const UserAddress = use('App/Models/UserAddress')
const geoCoder = NodeGeocoder({provider:'google',apiKey:'AIzaSyDMzoCDxB5o2oC88_H5-QJAmbc36W-9Ztk'})
const geoCoder2 = require('node-open-geocoder')
const ind = require('territory-indonesia')
class UserAddressController {
  /**
   * Show a list of all useraddresses.
   * GET useraddresses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    try{
      const params = request.all()
        let page = params.page ? params.page : 1
        let limit = params.limit ? params.limit : 5
        const offset = (page-1)*limit

      const data = await UserAddress.query()
        .where('user_id',request.decodeToken.id)
        .where('is_deleted', false)
        .paginate(page, limit)
      
      let json = data.toJSON()
      if(!json.data[0]) return response.ok('Data Not Found')
      
      json.data = json.data.map(item=>{
        let new_item = {}
        new_item.id = item.id
        new_item.nama_alamat = item.nama_alamat
        new_item.nama_penerima = item.nama_penerima
        new_item.phone = item.phone
        new_item.alamat_pengiriman = item.alamat
        new_item.daerah_pengiriman = item.kecamatan + ", " + item.kota + ", " + item.provinsi + ", " + item.kode_pos
        return new_item
      })
      return response.ok(json, 'list of user address') 
    }catch(err){
      console.log(err.message)
    }
  }

  async getList({request, response}){
    try{
      const params = request.all()

      const data = await UserAddress.query()
        .where('user_id',request.decodeToken.id)
        .where('is_deleted', false)
        .fetch()
      
      let json = data.toJSON()
      if(!json[0]) return response.ok('Data Not Found')
      
      json = json.map(item=>{
        let new_item = {}
        new_item.id = item.id
        new_item.nama_alamat = item.nama_alamat
        new_item.nama_penerima = item.nama_penerima
        new_item.phone = item.phone
        new_item.alamat_pengiriman = item.alamat
        new_item.daerah_pengiriman = item.kecamatan + ", " + item.kota + ", " + item.provinsi + ", " + item.kode_pos
        return new_item
      })
      return response.ok(json, 'list of user address') 
    }catch(err){
      console.log(err.message)
    }
  }

  /**
   * Render a form to be used for creating a new useraddress.
   * GET useraddresses/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new useraddress.
   * POST useraddresses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    let data = request.all()
    const checkAddress = await UserAddress.query().where({user_id:request.decodeToken.id,is_deleted:false}).first()
    if(!checkAddress) data.alamat_utama = true
    data.user_id = request.decodeToken.id
    const res = await UserAddress.create(data)
    return response.ok(res)
  }

  /**
   * Display a single useraddress.
   * GET useraddresses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const data = await UserAddress.find(params.id)
    return response.ok(data)
  }

  async showDetail ({params, request, response}) {
    let data = await UserAddress.find(params.id)
    const provinsi = data.provinsi
    const kota = data.kota
    const kecamatan = data.kecamatan

    const data_provinsi = await ind.getAllProvinces();
    const res_provinsi = data_provinsi.map(item=>{
      return item.name
    })

    data.provinsi = {
      array : res_provinsi,
      selected : res_provinsi.indexOf(provinsi)
    }

    const data_kota = await ind.getRegenciesOfProvinceName(provinsi)
    const res_kota = data_kota.map(item=>{
      return item.name
    })

    data.kota = {
      array : res_kota,
      selected : res_kota.indexOf(kota)
    }

    const data_kecamatan = await ind.getDistrictsOfRegencyName(kota)
    const res_kecamatan = data_kecamatan.map(item=>{
      return item.name
    })

    data.kecamatan = {
      array : res_kecamatan,
      selected : res_kecamatan.indexOf(kecamatan)
    }

    return response.ok(data)

  }

  /**
   * Render a form to update an existing useraddress.
   * GET useraddresses/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update useraddress details.
   * PUT or PATCH useraddresses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    try{
      const data = request.all()
      
      let userAddress = await UserAddress.find(params.id)
      if(!userAddress) throw 'User Address Not Found'
      if(userAddress.user_id!=request.decodeToken.id) throw 'Can\'t Update Other User\'s Address'
      userAddress.merge(data)
      await userAddress.save()
      return response.ok(userAddress, 'Update Success')
    }catch(err){
        return response.ok(err.message)
    }
  }

  /**
   * Delete a useraddress with id.
   * DELETE useraddresses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try{
      const res = await UserAddress.find(params.id)
      if(!res) return response.ok(null,'Address not found')
      res.merge({is_deleted:true})
      await res.save()
      return response.ok("Address deleted")
    }catch(err){
      console.log(err.message)
    }
  }

  async setAlamatUtama ({params, request, response}) {
    try{
      const data = request.all()
      await UserAddress.query()
        .where('user_id',request.decodeToken.id)
        .update( { alamat_utama:false } )

      const res = await UserAddress.query()
        .where('id', data.id)
        .update( { alamat_utama:true } )

      return response.ok(res)
    }catch(err){
        console.log(err.message)
    }
    
  }

  async getProvinces({request, response}){
    const data = await ind.getAllProvinces();
    const res = data.map(item=>{
      return item.name
    })

    return response.ok(res)
  }

  async getCities({request, response}){
    const {provinsi} = request.all()
    const data = await ind.getRegenciesOfProvinceName(provinsi)
    const res = data.map(item=>{
      return item.name
    })

    return response.ok(res)
  }

  async getDistricts({request, response}){
    const {kota} = request.all()
    
    const data = await ind.getDistrictsOfRegencyName(kota)
    const res = data.map(item=>{
      return item.name
    })

    return response.ok(res)
  }
}

module.exports = UserAddressController
