'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with productsubcategories
 */
const ProductSubCategory = use('App/Models/ProductSubCategory')
const CRUDService = use('App/Services/CRUDService')

class ProductSubCategoryController {
  /**
   * Show a list of all productsubcategories.
   * GET productsubcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new productsubcategory.
   * GET productsubcategories/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new productsubcategory.
   * POST productsubcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    let params = request.all()
    const data = await ProductSubCategory.create(params)
    return response.ok(data)
  }

  /**
   * Display a single productsubcategory.
   * GET productsubcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing productsubcategory.
   * GET productsubcategories/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update productsubcategory details.
   * PUT or PATCH productsubcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let data = request.all()
    const SubCategory = await ProductSubCategory.find(params.id)
    if(!SubCategory) return response.ok('Sub Category not Found')
    SubCategory.merge(data)
    await SubCategory.save()
    return response.ok(SubCategory)
  }

  /**
   * Delete a productsubcategory with id.
   * DELETE productsubcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try{
      let data = await CRUDService.destroy('App/Models/ProductSubCategory', params.id)

      return response.ok(null, 'Delete Success')
    }catch(err){
        return response.ok(err)
    }
  }
}

module.exports = ProductSubCategoryController
