'use strict'

const Payment = use('App/Models/PaymentMethod')
const ImageService = use('App/Services/ImageService')
const Env = use('Env')
class PaymentMethodController {
    async index({request, response, view}){
        const data = await Payment.query()
            .where('is_deleted', false)
            .fetch()
            let json = data.toJSON()
            json.content = json
        return view.render('pages.payments.index', {data:json})
    }

    async create({request, response, view}){
        return view.render('pages.payments.add')
    }

    async store({request, response}){
        let data = request.all()
        let icon = request.file('icon', {
        types: ['image'],
        size: '10mb'
        })
        const cloudinaryResponse = await ImageService.saveImage(icon, Env.get('CLOUDINARY_PAYMENT_METHOD_ICON_FOLDER'))
        data.icon = cloudinaryResponse.secure_url
        const payment_method = new Payment()
        payment_method.fill(data)
        await payment_method.save()
        return response.redirect('/admin/payment')
    }

    async edit({request, response, view, params}){
        const data = await Payment.find(params.id)

        return view.render('pages.payments.edit', {data})
    }

    async update({request, response, params}){
        const data = request.post()
        let icon = request.file('icon', {
            types: ['image'],
            size: '10mb'
        })
        if(icon){
            const cloudinaryResponse = await ImageService.saveImage(icon, Env.get('CLOUDINARY_PAYMENT_METHOD_ICON_FOLDER'))
            data.icon = cloudinaryResponse.secure_url
        }
        const payment = await Payment.find(params.id)
        
        payment.merge(data)
        await payment.save()

        return response.redirect('/admin/payment')
    }

    async destroy({request, response, params}){
        const payment = await Payment.find(params.id)
        payment.merge({is_deleted:true})
        await payment.save()

        return response.redirect('/admin/payment')
    }
}

module.exports = PaymentMethodController
