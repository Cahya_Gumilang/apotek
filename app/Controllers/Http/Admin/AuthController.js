'use strict'

const User = use('App/Models/User')
const JWTService = use('App/Services/JWTService')
const md5 = use('md5')
const Admin = use('App/Models/Admin')
class AuthController {
    async doLogin({request, response, auth, session}){
        try{
            const {email, password} = request.all()
            await auth.attempt(email, password)
            return response.redirect('/admin/dashboard')
        }catch(err){
            console.log(err.message)
            session.withErrors({ error: "Wrong Username or Password" }).flashAll()
            return response.redirect('/admin/login')
        }
    }

    async show({request, response}){
        return response.ok(request.decodeToken)
    }
    
    async doLogout({request, response, auth}){
        await auth.logout()
        return response.redirect('/admin/login')
    }

    async store({request, response}){
        try{
            const data = request.all()
            const photo = request.file('photo', {
                types: ['image'],
                size: '10mb'
            })
            if(photo){
                const cloudinaryResponse = await ImageService.saveImage(photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                data.photo = cloudinaryResponse.secure_url
            }

            let user = await CRUDService.store('App/Models/Admin', data)

            return response.ok(user, 'User was created')
        }catch(err){
            return response.ok(err)
        }
    }
}

module.exports = AuthController
