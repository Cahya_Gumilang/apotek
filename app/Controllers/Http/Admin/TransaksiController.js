'use strict'

const Transaksi = use('App/Models/Transaksi')
const Status = use('App/Models/StatusTransaksi')
const moment = require('moment')
class TransaksiController {
    async index({request, response, view}){
        const data = await Transaksi.query()
            .with('user')
            .with('status')
            .with('detail_transaksi')
            .fetch()

        let json = data.toJSON()
        json.content = json.map(item=>{
            let m = moment(item.tgl_transaksi)
            item.tgl_transaksi = m.format('DD MMMM YYYY HH:mm:ss')
            item.total = 0
            
            item.detail_transaksi.forEach(item2=>{
                let total = (item2.price-(item2.price*item2.discount/100))*item2.qty
                item.total += total
            })

            return item
        })
        
        return view.render('pages.transactions.index', {data:json})
    }

    async show({request, response, params, view}){
        let data = await Transaksi.query()
        .where('id',params.id)
        .with('user')
        .with('admin')
        .with('detail_transaksi.product')
        .with('courier')
        .with('payment_method')
        .with('status')
        .with('user_address')
        .first()

    data = data.toJSON()
      let res = {
        id : data.id,
        invoice : data.invoice,
        tgl_transaksi : data.tgl_transaksi,
        status : data.status.current_status,
        point : data.point,
        courier: data.courier.name,
        user: data.user_address.nama_penerima,
        username: data.user.firstname+" "+data.user.lastname,
        ongkir : 11000,
        payment : data.payment_method.name,
        total_bayar : 11000,
        total_product : 0,
        alamat : data.user_address.alamat + ', ' + data.user_address.kecamatan + ', ' + data.user_address.kota + ', ' + data.user_address.provinsi + ', ' + data.user_address.kode_pos,
        no_telp : data.user_address.phone
      }

      if(res.status=="dikirim"){
        res.estimasi = moment(data.tgl_transaksi).add(3,'days').format('DD MMM YYYY')
        res.tgl_dikirim = data.status.dikirim
        res.pengirim = data.admin.firstname+" "+data.admin.lastname
      }

      else if(res.status=="diterima"){
        res.tgl_dikirim = data.status.dikirim
        res.tgl_diterim = data.status.diterima
        res.pengirim = data.admin.firstname+" "+data.admin.lastname
      }

      data.detail_transaksi.forEach(item2=>{
        res.total_bayar +=  (item2.price - (item2.price*item2.discount/100))*item2.qty
        res.total_product++
      })

      res.product = data.detail_transaksi.map(detail=>{
        let detail2 = {
          id : detail.product.id,
          name : detail.product.name,
          thumbnail : detail.product.thumbnail,
          price : detail.price,
          discount : detail.discount,
          disc_price : (detail.price - (detail.price*detail.discount/100)),
          qty : detail.qty+" Produk",
          total_harga : (detail.price - (detail.price*detail.discount/100))*detail.qty
        }

        return detail2
      })

      return view.render('pages.transactions.detail', {data:res})
    }

    async sendConfirm({request, response, params}){
        const status = await Status.findBy('transaksi_id', params.id)
        status.merge({ dikirim : new Date(), current_status:'dikirim' })
        await status.save()

        return response.redirect('/admin/transaction')
    }
}

module.exports = TransaksiController
