'use strict'
const Courier = use('App/Models/Courier')
class CourierController {
    async index({request, response, view}){
        const data = await Courier.query()
            .where('is_deleted', false)
            .fetch()
        let json = data.toJSON()
        json.content = json
        return view.render('pages.couriers.index', {data:json})
    }

    async create({request, response, view}){
        return view.render('pages.couriers.add')
    }

    async store({request, response}){
        const data = request.post()
        const courier = await Courier.create(data)
        return response.redirect('/admin/courier')
    }

    async edit({request, response, view, params}){
        const data = await Courier.find(params.id)

        return view.render('pages.couriers.edit', {data})
    }

    async update({request, response, params}){
        const data = request.post()
        const courier = await Courier.find(params.id)
        courier.merge(data)
        await courier.save()

        return response.redirect('/admin/courier')
    }

    async destroy({request, response, params}){
        const courier = await Courier.find(params.id)
        courier.merge({is_deleted:true})
        await courier.save()

        return response.redirect('/admin/category')
    }
}

module.exports = CourierController
