'use strict'

const User = use('App/Models/User') 
const CRUDService = use('App/Services/CRUDService')
const md5 = require('md5')
const MailerService = use('App/Services/MailerService')
const Env = use('Env')
const ImageService = use('App/Services/ImageService');
const DataTables = require('datatables.net')
const moment = require('moment')
class UserController {
    async index({request, response, view}) {
        const params = request.all()

        const data = await User.query()
            .where('is_deleted', false)
            .fetch()
        
        let json = data.toJSON()
        json.content = json
        json.content = json.content.map(item=>{
            let m = moment(item.dob)
            item.dob = m.format('DD MMMM YYYY')
            return item
        })

        return view.render('pages.users.index', {users:json}) 
    }

    async show({request, response, params, view}) {
       
            let data = await User.query()
                .where('id', params.id)
                .with('user_addresses')
                .with('carts.product')
                .with('wish_lists.product')
                .with('transaksi', builder=>{
                    builder.with('status')
                    .with('detail_transaksi')
                })
                .first()
    
            data = data.toJSON()
            data.transaksi = data.transaksi.map(item=>{
                let m = moment(item.tgl_transaksi)
                item.tgl_transaksi = m.format('DD MMMM YYYY HH:mm:ss')
                item.total = 0
                
                item.detail_transaksi.forEach(item2=>{
                    let total = (item2.price-(item2.price*item2.discount/100))*item2.qty
                    item.total += total
                })
    
                return item
            })
            return view.render('pages.users.detail', {data})
       
    }

    async create({request, response, view}){
        return view.render('pages.users.add')
    }

    async store({request, response}) {
        try{
            const data = request.all()
            console.log(data)
            const photo = request.file('photo', {
                types: ['image'],
                size: '10mb'
            })
            if(photo){
                const cloudinaryResponse = await ImageService.saveImage(photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                data.photo = cloudinaryResponse.secure_url
            }
            let check_user = await User.findBy('email', data.email)
            if(check_user){
                session.withErrors({ email: "Email Already Taken" }).flashAll()
                return response.redirect('/admin/user/create')
            }
            data.status = true
            let user = await User.create(data)
            return response.redirect('/admin/user')
        }catch(err){
            return response.ok(err)
        }
    }

        async edit ({ params, request, response, view}) {
            const data = await User.find(params.id)
            let m = moment(data.dob)
            data.dob = m.format('YYYY-MM-DD')
            return view.render('pages.users.edit', {data})
        }

    async update({request, response, params}) {
        try{
            const data = request.post()
            const photo = request.file('photo', {
                types: ['image'],
                size: '10mb'
            })
            let user = await User.find(data.id)
            if(!user) throw 'User Not Found'

            if(photo){
                const cloudinaryResponse = await ImageService.saveImage(photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                data.photo = cloudinaryResponse.secure_url
            }

            user.merge(data)
            await user.save()

            return response.redirect('/admin/user')
        }catch(err){
            return response.ok(err)
        }
    }

    async destroy({request, response, params}) {   
        try{
            let user = await User.find(params.id)
            user.merge({is_deleted:true})
            await user.save()

            return response.redirect('/admin/user')
           
        }catch(err){

        }
    }

}

module.exports = UserController
