'use strict'

class LayoutController {
    async dashboard({view}){
        return view.render('pages.dashboard',{data:{
            chartdata : [0, 10000, 5000, 15000, 10000, 20000, 15000, 25000, 20000, 30000, 25000, 40000]
        }})
    }
    async user({view}){
        return view.render('pages.user')
    }
    async login({view, auth, response}){
        try{
            await auth.check()
            return response.redirect('/admin/dashboard')
        }catch(err){
            return view.render('forms.login')
        }
        
    }
}

module.exports = LayoutController
