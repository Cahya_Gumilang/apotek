'use strict'
const User = use('App/Models/User')
const Product = use('App/Models/Product')
const Transaksi = use('App/Models/Transaksi')
const Database = use('Database')
class DashboardController {
    async index ({request, response, view}) {
        const user = await User.query()
            .where('is_deleted', false)
            .count('* as total')

        const product = await Product.query()
            .where('is_deleted', false)
            .where('stok','>',0)
            .count('* as total')
        const year = new Date().getFullYear()

        const transaksi_query = Transaksi.query()
            .where('tgl_transaksi','like',year+'%')
            .with('detail_transaksi')
        
        const transaksi = await transaksi_query.fetch()
        const transaksi_count = await transaksi_query.count("* as total")

        let chartdata = new Array(12).fill(0)
        let earnings_annual = 0
        transaksi.toJSON().forEach(item=>{
            item.detail_transaksi.forEach(item2=>{
                let total = (item2.price-(item2.price*item2.discount/100))*item2.qty
                let month = new Date(item2.created_at).getMonth()
                console.log(month)
                chartdata[month] += total
                earnings_annual += total
            })
        })
        console.log(transaksi_count[0].total)
           
        return view.render('pages.dashboard',{data:{
            total_users : user[0].total,
            total_products : product[0].total,
            chartdata,
            total_transactions : transaksi_count[0].total,
            earnings_annual
        }})
    }
}

module.exports = DashboardController
