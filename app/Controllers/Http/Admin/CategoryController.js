'use strict'
const Category = use('App/Models/ProductCategory')
const SubCategory = use('App/Models/ProductSubCategory')
const ImageService = use('App/Services/ImageService')
const Env = use('Env')
class CategoryController {
    async index({request, response, view}){
        const data = await Category.query()
            .where('is_deleted', false)
            .with('sub_categories', builder=>{
                builder.where('is_deleted', false)
            })
            .fetch()
            let json = data.toJSON()
            json.content = json
        return view.render('pages.categories.index', {data:json})
    }

    async create({request, response, view}){
        return view.render('pages.categories.add')
    }

    async store({request, response}){
        let data = request.all()
        let icon = request.file('icon', {
          types: ['image'],
          size: '10mb'
        })
        const cloudinaryResponse = await ImageService.saveImage(icon, Env.get('CLOUDINARY_PRODUCT_CATEGORY_ICON_FOLDER'))
        data.icon = cloudinaryResponse.secure_url
        const product_category = new Category()
        product_category.fill(data)
        await product_category.save()

        return response.redirect('/admin/category')
    }

    async edit({request, response, params, view}){
        const data = await Category.query()
            .where('id', params.id)
            .with('sub_categories', builder=>{
                builder.where('is_deleted', false)
            })
            .first()
        
        const json = data.toJSON()

        return view.render('pages.categories.edit', {data:json})
    }

    async update({request, response, params}){
        const data = request.post()
        const category = await Category.find(params.id)
        category.merge(data)
        await category.save()

        return response.redirect('/admin/category')

    }

    async createSub({request, response, view, params}){
        const data = await Category.find(params.id)
        return view.render('pages.categories.add_sub', {data})
    }

    async storeSub({request, response, params}){
        const data = request.post()
        data.product_category_id = params.id
        const sub = await SubCategory.create(data)

        return response.redirect('/admin/category')
    }

    async destroySub({request, response, params}){
        const data = request.all()
        const sub = await SubCategory.find(params.id)
        sub.merge({is_deleted:true})
        await sub.save()

        return response.redirect('/admin/category/'+data.product_category_id+'/edit')
    }

    async destroy({request, response, params}){
        const category = await Category.find(params.id)
        category.merge({is_deleted:true})
        await category.save()

        return response.redirect('/admin/category')
    }
}

module.exports = CategoryController
