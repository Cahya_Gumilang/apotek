'use strict'


const Admin = use('App/Models/Admin')
const moment = require('moment')
class AdminController {
    async index({request, response, view}) {

        const data = await Admin.query()
            .where('is_deleted', false)
            .with('functions')
            .fetch()
        
        let json = data.toJSON()
        json.content = json
        json.content = json.content.map(item=>{
            let m = moment(item.dob)
            item.dob = m.format('DD MMMM YYYY')
            return item
        })
        return view.render('pages.admins.index', {admins:json}) 
    }

    async create({request, response, view}){
        return view.render('pages.admins.add')
    }

    async store({request, response, view}){
        try{
            const data = request.all()
            console.log(data)
            const photo = request.file('photo', {
                types: ['image'],
                size: '10mb'
            })
            if(photo){
                const cloudinaryResponse = await ImageService.saveImage(photo, Env.get('CLOUDINARY_USER_PROFILE_FOLDER'))
                data.photo = cloudinaryResponse.secure_url
            }
            let check_admin = await Admin.findBy('email', data.email)
            if(check_admin){
                session.withErrors({ email: "Email Already Taken" }).flashAll()
                return response.redirect('/admin/admin/create')
            }
            data.status = true
            let admin = await Admin.create(data)
            return response.redirect('/admin/admin')
        }catch(err){
            return response.ok(err)
        }
    }

    async edit ({ params, request, response, view}) {
        const data = await Admin.find(params.id)
        let m = moment(data.dob)
        data.dob = m.format('YYYY-MM-DD')
        return view.render('pages.admins.edit', {data})
    }

    async update({request, response, params}) {
        const data = request.all()
        console.log(data)
        data.manage_admins = data.manage_admins ? data.manage_admins : 0
        data.manage_users = data.manage_users ? data.manage_uesers : 0
        data.manage_products = data.manage_products ? data.manage_products : 0
        data.manage_transactions = data.manage_transactions ? data.manage_transactions : 0
        delete data._method
        const admin = await Admin.find(params.id)
        admin.merge(data)
        await admin.save()

        return response.redirect('/admin/admin')
    }

    async updateManage({request, response, params}){
        const data = request.all()
        const admin = await Admin.find(params.id)
        admin.merge(data)
        await admin.save()

        return response.ok(admin)
    }

    async destroy({request, response, params, view}) {
        const admin = await Admin.find(params.id)
        admin.merge({is_deleted:true})
        await admin.save()
        return response.redirect('/admin/admin')
    }
}

module.exports = AdminController
