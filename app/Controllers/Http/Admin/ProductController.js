'use strict'

const Product = use('App/Models/Product')
const Category = use('App/Models/ProductCategory')
const ImageService = use('App/Services/ImageService')
const Env = use('Env')
const ProductHasSubCategory = use('App/Models/ProductHasSubCategory')
const ProductImage = use('App/Models/ProductImage')
class ProductController {
    async index({request, response, view}) {
        const params = request.all()
        let page = params.page ? params.page : 1
        let limit = params.limit ? params.limit : 10
        const offset = (page-1)*limit

        const data = await Product.query()
            .where('is_deleted', false)
            .with('sub_categories.category')
            .fetch()
        
        let json = data.toJSON()
        json.content = json
        let category = []
        json.content = json.content.map(item=>{
            item.category = item.sub_categories.map(item2=>{
                if(!category.includes(item2.category.id)){
                    category.push(item2.category.id)
                    return item2.category.name + "<br>"
                }
            })
            category = []

            return item
        })

        return view.render('pages.products.index', {products:json}) 
    }

    async show({request, response, view, params}){
    let data = await Product.query()
        .where('id', params.id)
        .where('is_deleted', false)
        .with('sub_categories', builder=>{
            builder.with('category')
        })
        .with('product_images')
        .fetch()
        data = data.toJSON()[0]

        data.category = Array.from(new Set(data.sub_categories.map(item=>{
            return item.category.id        
        })))

        let category = await Category.query().where('id', data.category).fetch()
        
        data.category = category.toJSON()

        return view.render('pages.products.detail', {data}) 
    }

    async create({request, response, view}) {
        let category = await Category.query().with('sub_categories').fetch()
        category = category.toJSON()

        console.log(category)

        return view.render('pages.products.add', { data : {category} })
    }

    async store({request, response}){
        try{
            let data = request.except(['sub_category'])
            let sub_category = request.only(['sub_category']).sub_category
            console.log(sub_category)
            let thumbnail = request.file('thumbnail', {
              types: ['image'],
              size: '10mb'
            })
            let images = request.file('image', {
              types: ['image'],
              size: '10mb'
            })
            const cloudinaryThumbanil = await ImageService.saveImage(thumbnail, Env.get('CLOUDINARY_PRODUCT_THUMBNAIL_FOLDER'))
            data.thumbnail = cloudinaryThumbanil.secure_url
            const product = new Product()
            product.fill(data)
            await product.save()
      
            let sub_category_arr = sub_category.map(item=>{
                return {
                    product_id : product.id,
                    product_sub_category_id : item

                }
            })
            await ProductHasSubCategory.createMany(sub_category_arr)
            let image = []
            for(const file of images._files){
              let datares = await ImageService.saveImage(file, Env.get('CLOUDINARY_PRODUCT_IMAGE_FOLDER'))
              image.push(datares.secure_url)
            }
      
            let data_image = []
            image.forEach(item=>{
              data_image.push({product_id:product.id, image:item})
            })
      
            const product_images = await ProductImage.createMany(data_image)
            product.image = product_images
      
            
            return response.redirect('/admin/product')
          }catch(err){
            return response.ok(null, err.message, 500)
          }
    }

    async edit({request, response, params, view}){
        let category = await Category.query().with('sub_categories').fetch()
        category = category.toJSON()

        let product = await Product.query()
            .where('id', params.id)
            .with('sub_categories')
            .with('product_images')
            .first()
        product = product.toJSON()
        console.log(product.sub_categories)
        product.category = category

        // return response.ok(product)

        return view.render('pages.products.edit', {data:product})
        
    }

    async update({request, response, params}){
         try{
            let data = request.except(['sub_category','_method'])
            let sub_category = request.only(['sub_category']).sub_category
            console.log(sub_category)
            let thumbnail = request.file('thumbnail', {
              types: ['image'],
              size: '10mb'
            })
            let images = request.file('image', {
              types: ['image'],
              size: '10mb'
            })
            const product = await Product.find(params.id)
            if(thumbnail){
                const cloudinaryThumbanil = await ImageService.saveImage(thumbnail, Env.get('CLOUDINARY_PRODUCT_THUMBNAIL_FOLDER'))
                data.thumbnail = cloudinaryThumbanil.secure_url
            }
        
            product.merge(data)
            await product.save()
      
            let sub_category_arr = sub_category.map(item=>{
                return {
                    product_id : product.id,
                    product_sub_category_id : item

                }
            })

            await ProductHasSubCategory.createMany(sub_category_arr)
            if(images){
            let image = []
            for(const file of images._files){
              let datares = await ImageService.saveImage(file, Env.get('CLOUDINARY_PRODUCT_IMAGE_FOLDER'))
              image.push(datares.secure_url)
            }
      
            let data_image = []
            image.forEach(item=>{
              data_image.push({product_id:product.id, image:item})
            })
      
            const product_images = await ProductImage.createMany(data_image)
        }
      
            
            return response.redirect('/admin/product')
          }catch(err){
            return response.ok(null, err.message, 500)
          }
    }

    async destroy({request, response, params}){
        const product = await Product.find(params.id)
        product.merge({is_deleted:true})
        await product.save()

        return response.redirect('/admin/product')
    }

    async destroySub({request, response, params}){
        const data = request.get()
        const sub = await ProductHasSubCategory.find(data.id)
        await sub.delete()

        return response.redirect('/admin/product/'+data.back_id+'/edit')
    }

    async destroyImage({request, response, params}){
        const data = request.get()
        const image = await ProductImage.find(data.id)
        await image.delete()

        return response.redirect('/admin/product/'+data.back_id+'/edit')
    }
}

module.exports = ProductController
