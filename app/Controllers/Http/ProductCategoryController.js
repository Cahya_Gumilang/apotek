'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with productcategories
 */
const ProductCategory = use('App/Models/ProductCategory')
const ImageService = use('App/Services/ImageService')
const Env = use('Env')
const CRUDService = use('App/Services/CRUDService')
class ProductCategoryController {
  /**
   * Show a list of all productcategories.
   * GET productcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
     const data = await ProductCategory.query()
      .where('is_deleted', false)
      .with('sub_categories')
      .fetch()
     return response.ok(data) 
  }

  /**
   * Render a form to be used for creating a new productcategory.
   * GET productcategories/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new productcategory.
   * POST productcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    let data = request.all()
    let icon = request.file('icon', {
      types: ['image'],
      size: '10mb'
    })
    const cloudinaryResponse = await ImageService.saveImage(icon, Env.get('CLOUDINARY_PRODUCT_CATEGORY_ICON_FOLDER'))
    data.icon = cloudinaryResponse.secure_url
    const product_category = new ProductCategory()
    product_category.fill(data)
    await product_category.save()
    return response.ok(data)
  }

  async listCategoryHasSub ({request, response}){
    const data = await ProductCategory.query()
      .where('is_deleted', false)
      .with('sub_categories')
      .has('sub_categories')
      .fetch()
    console.log(data)
    return response.ok(data) 
  }

  /**
   * Display a single productcategory.
   * GET productcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing productcategory.
   * GET productcategories/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update productcategory details.
   * PUT or PATCH productcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    try{
      const data = request.all()
      const icon = request.file('icon', {
          types: ['image'],
          size: '10mb'
      })
      let category = await ProductCategory.find(params.id)
      if(!category) throw 'Category Not Found'

      if(icon){
          const cloudinaryResponse = await ImageService.saveImage(icon, Env.get('CLOUDINARY_PRODUCT_CATEGORY_ICON_FOLDER'))
          data.icon = cloudinaryResponse.secure_url
          const oldPublicId = ImageService.getPublicId(category.icon, Env.get('CLOUDINARY_PRODUCT_CATEGORY_ICON_FOLDER'))
          await ImageService.destroyImage(oldPublicId)
      }

      category.merge(data)
      await category.save()

      return response.ok(category, 'Update Success')
    }catch(err){
        return response.ok(err.message)
    }
  }

  /**
   * Delete a productcategory with id.
   * DELETE productcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try{
      let data = await CRUDService.destroy('App/Models/ProductCategory', params.id)

      const publicId = ImageService.getPublicId(data.icon, Env.get('CLOUDINARY_PRODUCT_CATEGORY_ICON_FOLDER'))
      await ImageService.destroyImage(publicId)

      return response.ok(null, 'Delete Success')
    }catch(err){
        return response.ok(err)
    }
  }
}

module.exports = ProductCategoryController
