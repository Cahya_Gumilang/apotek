'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with products
 */
const Product = use('App/Models/Product')
const ProductImage = use('App/Models/ProductImage')
const Env = use('Env')
const ImageService = use('App/Services/ImageService')
const ProductSubCategory = use('App/Models/ProductSubCategory')
const ProductHasSubCategory = use('App/Models/ProductHasSubCategory')
const CRUDService = use('App/Services/CRUDService')
class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
        const params = request.all()
        

        let data = await CRUDService.getRecordWithRelation('App/Models/Product', {},{},['product_images', 'sub_categories.category', 'promotions'])
        data.data = data.data.map(item=>{
          if(item.discount>0){
            item.price = item.price - (item.price*item.discount/100)  
          }
          // item.discount_price = item.price - (item.price*item.discount/100)
          item.discount = item.discount
          return item
        })

        return response.ok(data)
  }
  /**
   * 
   * @param {*} param0 
   */
  async listProduct ({ request, response}) {
    const params = request.all()

    let data = await CRUDService.getRecordWithRelation('App/Models/Product', params,{},[],[], 'id,name,thumbnail,price,stok,discount' )
    data.data = data.data.map(item=>{
      if(item.discount>0){
        item.price = item.price - (item.price*item.discount/100)  
      }
      // item.discount_price = item.price - (item.price*item.discount/100)
      item.discount = item.discount
      return item
    })

    return response.ok(data, 'list of product') 
  }

  async detail ({request, response, params}) {
    const data = await CRUDService.getSingleRecordWithRelation('App/Models/Product', params.id, ['product_images'])
    
    data.discount_price = data.price - (data.price*data.discount/100)
    data.discount = data.discount
    delete data.created_at
    delete data.updated_at
    delete data.is_deleted

    return response.ok(data)
  }

  async listByCategory ({request, response, params}) {
    try{
      const category_id = params.id
      let data = await Product.query()
        .select(['id','name','thumbnail','price','stok','discount'])
        .where('is_deleted', false)
        .whereHas('sub_categories', (builder)=>{
          builder.whereHas('category', (builder2)=>{
            builder2.where('id', category_id)
          })
        })
        .limit(8)
        .fetch()
      const count = await Product.query()
        .where('is_deleted', false)
        .whereHas('sub_categories', (builder)=>{
          builder.whereHas('category', (builder2)=>{
            builder2.where('id', category_id)
          })
        }).count('* as total')
        
        data = data.toJSON().map((item)=>{
          if(item.discount>0){
            item.price = item.price - (item.price*item.discount/100)  
          }
          // item.discount_price = item.price - (item.price*item.discount/100)
          item.discount = item.discount
          return item
        })
        
      return response.ok({
        total : count[0].total,
        data
      })
    }catch(err){
      response.ok(null, err.message, 500)
    }
  }

  async listByCategoryAll ({request, response, params}) {
    try{
      const data_params = request.all()
      const category_id = params.id
      const page = data_params.page ? data_params.page : 1
      const limit = data_params.limit ? data_params.limit : 6

      let data = await Product.query()
        .select(['id','name','thumbnail','price','stok','discount'])
        .where('is_deleted', false)
        .whereHas('sub_categories', (builder)=>{
          builder.whereHas('category', (builder2)=>{
            builder2.where('id', category_id)
          })
        })
        .paginate(page, limit)

       data.data = data.toJSON().data.map(item=>{
          if(item.discount>0){
            item.price = item.price - (item.price*item.discount/100)  
          }
          // item.discount_price = item.price - (item.price*item.discount/100)
          item.discount = item.discount
          return item
        })

      return response.ok(data)
    }catch(err){
      return response.ok(null, err.message, 500)
    }
  }

  async listBySubCategory ({request, response, params}) {
    try{
      const data_params = request.all()
      const sub_category_id = params.id
      const page = data_params.page ? data_params.page : 1
      const limit = data_params.limit ? data_params.limit : 6

      let data = await Product.query()
        .select(['id','name','thumbnail','price','stok','discount'])
        .where('is_deleted', false)
        .whereHas('sub_categories', (builder)=>{
          builder.where('product_sub_categories.id', sub_category_id)
        }).paginate(page, limit)
      data.data = data.toJSON().data.map(item=>{
          if(item.discount>0){
            item.price = item.price - (item.price*item.discount/100)  
          }
          // item.discount_price = item.price - (item.price*item.discount/100)
          item.discount = item.discount
          return item
        })
      return response.ok(data)
    }catch(err){
      response.ok(null, err.message, 500)
    }
  }
  
  /**
   * Render a form to be used for creating a new product.
   * GET products/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    try{
      let data = request.except(['sub_category'])
      let sub_category = request.only(['sub_category']).sub_category
      console.log(sub_category)
      let thumbnail = request.file('thumbnail', {
        types: ['image'],
        size: '10mb'
      })
      let images = request.file('image', {
        types: ['image'],
        size: '10mb'
      })
      const cloudinaryThumbanil = await ImageService.saveImage(thumbnail, Env.get('CLOUDINARY_PRODUCT_THUMBNAIL_FOLDER'))
      data.thumbnail = cloudinaryThumbanil.secure_url
      const product = new Product()
      product.fill(data)
      await product.save()

      await ProductHasSubCategory.create(
        {
          product_id : product.id,
          product_sub_category_id : sub_category
        }
      )
      let image = []
      for(const file of images._files){
        let datares = await ImageService.saveImage(file, Env.get('CLOUDINARY_PRODUCT_IMAGE_FOLDER'))
        image.push(datares.secure_url)
      }

      let data_image = []
      image.forEach(item=>{
        data_image.push({product_id:product.id, image:item})
      })

      const product_images = await ProductImage.createMany(data_image)
      product.image = product_images

      
      return response.ok(product)
    }catch(err){
      return response.ok(null, err.message, 500)
    }

  }

  /**
   * Display a single product.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing product.
   * GET products/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    
  }

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    try{
      let data = await CRUDService.destroy('App/Models/ProductCategory', params.id)
      let data_images = await ProductImage.query().where('product_id', params.id).fetch()

        const publicId = ImageService.getPublicId(data.thumbnail, Env.get('CLOUDINARY_PRODUCT_THUMBNAIL_FOLDER'))
        await ImageService.destroyImage(publicId)


        var publicIdImage = []
        data_images.forEach(item=>{
          publicIdImage.push(ImageService.getPublicId(item.image, Env.get('CLOUDINARY_PRODUCT_IMAGE_FOLDER')))
        })

        for(let id in publicIdImage){
          await ImageService.destroyImage(id)
        }

      return response.ok(null, 'Delete Success')
    }catch(err){
        return response.ok(err.message)
    }
  }

  async search ({params, request, response}) {
    try{
      const params = request.all()
      const category = params.category ? params.category : 0
      const sub_category = params.sub_category ? params.sub_category : 0
      let data = await CRUDService.getRecordWithRelation('App/Models/Product', params,{},['sub_categories.category'],['name','detail'], 'id,name,thumbnail,price,stok,discount' )
      let res = data.data.map(item=>{
        if(item.discount>0){
          item.price = item.price - (item.price*item.discount/100)  
        }
        // item.discount_price = item.price - (item.price*item.discount/100)
        item.discount = item.discount
        return item
      })

      return response.ok(res, 'list of product') 
    }catch(err){

    }
  }
}

module.exports = ProductController
