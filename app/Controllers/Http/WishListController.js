'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with wishlists
 */
const WishList = use('App/Models/WishList')
const Cart = use('App/Models/Cart')
const Product = use('App/Models/Product')
class WishListController {
  /**
   * Show a list of all wishlists.
   * GET wishlists
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const user_id = request.decodeToken.id
    const data = await WishList.query()
      .where('user_id',user_id)
      .with('product', builder=>{
        builder.select(['id','name','thumbnail','price','stok','discount'])
      })
      .fetch()

      const res = data.toJSON().map(item=>{
        if(item.product.discount>0){
          item.product.price = item.product.price - (item.product.price*item.product.discount/100)
        }
        item.total_price = "Rp"+(item.qty * item.product.price)
        item.product.price = "Rp"+item.product.price
        // item.product.discount_price = item.product.price - (item.product.price*item.product.discount/100)
        item.product.discount = item.product.discount
        return item
      })
  
      return response.ok(res)
  }

  /**
   * Render a form to be used for creating a new wishlist.
   * GET wishlists/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new wishlist.
   * POST wishlists
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const params = request.all()
    const product_id = params.product_id
    const user_id = request.decodeToken.id
    const res = await WishList.query().where({product_id, user_id}).first()
    if(!res){
       await WishList.create({product_id, user_id})
    }
    return response.ok(null, 'Dimasukkan ke WishList')
  }

  /**
   * Display a single wishlist.
   * GET wishlists/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing wishlist.
   * GET wishlists/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update wishlist details.
   * PUT or PATCH wishlists/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a wishlist with id.
   * DELETE wishlists/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    if(params.id == "all"){
      await WishList.query()
              .where('user_id',request.decodeToken.id)
              .delete()
    } 
    else{
      let wishList = await WishList.find(params.id)
      await wishList.delete()
    }
    return response.ok(null, 'Deleted from WishList')
  }


  async insertAllToCart ({params, request, response}) {
    const user_id = request.decodeToken.id
    const query = WishList.query()
      .where('user_id',user_id)
      .with('product', builder=>{
        builder.select(['id'])
      })

    const data = await query.fetch()

    for(let item of data.toJSON()){
      let product_id = item.product.id
      let qty = 1
      let res = await Cart.query().where({product_id, user_id}).first()
      if(res) {
        qty += res.qty
        await Cart.query().where({product_id, user_id}).update({qty})
      }
      else await Cart.create({product_id, user_id, qty})
    }

    await query.delete()
    

    return response.ok(null, "Success")
  }

  async insertToCart ({params, request, response}) {
    const user_id = request.decodeToken.id
    const data = WishList.find(params.id)
    

      let product_id = data.product_id
      let qty = 1
      let res = await Cart.query().where({product_id, user_id}).first()
      if(res) {
        qty += res.qty
        await Cart.query().where({product_id, user_id}).update({qty})
      }
      else await Cart.create({product_id, user_id, qty})
    
    await data.delete()

    return response.ok(null, "Success")
  }

  async getRelatedProduct ({request, response}) {
    try{
      const user_id = request.decodeToken.id
      const data = await WishList.query()
        .where('user_id',user_id)
        .with('product', builder=>{
          builder.select(['id'])
            .with('sub_categories.category')
        }).fetch()
      if(!data.toJSON()[0]) return response.ok(null, 'wishlist is empty')
      console.log(data.toJSON()[0].product)
      let category = []
      let sub_category = []
      for(let item of data.toJSON()){
        for(let item2 of item.product.sub_categories){
          category.push(item2.category.id)
          sub_category.push(item2.id)
        }
      }

      const category_id = Array.from(new Set(category))
      const sub_category_id = Array.from(new Set(sub_category))

        let res = await Product.query()
          .select(['id','name','thumbnail','price','stok','discount'])
          .where('is_deleted', false)
          .whereHas('sub_categories', (builder)=>{
            builder.orWhereIn('product_sub_categories.id', sub_category_id)
              .whereHas('category', (builder2)=>{
                builder2.orWhereIn('product_categories.id', category_id)
              })
          })
          .limit(12)
          .fetch()
        
        if(!res.toJSON()[0]) return response.ok(null, 'can\'t find any related product' )

          res = res.toJSON().map((item)=>{
            if(item.discount>0){
              item.price = item.price - (item.price*item.discount/100)  
            }
            // item.discount_price = item.price - (item.price*item.discount/100)
            item.discount = item.discount
            return item
          })
          
        return response.ok(res)
      }catch(err){
        console.log(err.message)
      }
  }

}


module.exports = WishListController
