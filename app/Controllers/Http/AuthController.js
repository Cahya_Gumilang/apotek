'use strict'

const User = use('App/Models/User')
const CRUDService = use('App/Services/CRUDService')
const JWTService = use('App/Services/JWTService')
const Hash = use('Hash')
const md5 = use('md5')
const UtilityService = use('App/Services/UtilityService')
const Env = use('Env')
const MailerService = use('App/Services/MailerService')
class AuthController {
    async doLogin({request, response}){
        const req = request.all()
        
        let payload = await User.query().setHidden(['created_at','updated_at']).where('email', req.email).fetch()
        
        payload = payload.toJSON()
        payload = payload[0]
        
        if(!payload || !payload.status) return response.ok(null, 'Email not Found', 401)
        const isPasswordValid = await Hash.verify(req.password, payload.password)
        if(!isPasswordValid) return response.ok(null, 'Wrong Password', 401)

        delete payload.password
        
        const token = await JWTService.generateToken(payload)
        return response.ok({access_token:token, access_user:payload})
    }

    async doSocialLogin ({request, response, auth, ally, params}) {
        const provider = params.provider
        try {
            const userData = await ally.driver(provider).getUser() 
            const authUser = await User.query().where({
                'provider': provider,
                'provider_id': userData.getId()
            }).first() 
            if (!(authUser === null)) {
                await auth.loginViaId(authUser.id) 
                return response.redirect('/')
            }
            const user = new User() 
                user.firstname = userData.getName().substr(0, userData.getName().indexOf(' ')) 
                user.lastname = userData.getName().substr(userData.getName().indexOf(' ')+1)
                user.email = userData.getEmail() 
                user.provider_id = userData.getId() 
                user.photo = userData.getAvatar() 
                user.provider = provider 
            await user.save() 
            const token = await JWTService.generateToken(user)
            return response.ok({access_token:token, access_user:user})
        } catch (e) {
            console.log(e) 
            return response.ok(e.message)
        }

    }

    async show({request, response}){
        return response.ok(request.decodeToken)
    }

    async verifyEmail({request, response, params, view}){
        try{
            let token = params.token
            let data = await User.findBy('token', token)
            if(data){
                await CRUDService.updateAttribute('App/Models/User', data.id, {status:true})
                return view.render('pages.message', {
                    data:{
                        head : ':)',
                        message : 'SUCCESS',
                        sub_message : 'Your Email has Been Verified'
                    }
                })
            }
            
            return response.ok(null, 'bad Request', 400)
        }catch(err){
            console.log(err)
            return response.ok(err, 'Internal Server Error', 500)
        }
    }

    async register({request, response}){
        try{
        let data = request.all()
        const token = UtilityService.generateRandomToken()+new Date().getDate()+new Date().getDay()
            
        let user = new User()
        let userByEmail = await User.findBy('email', data.email)
        if(userByEmail){
            if(userByEmail.status){
                throw 'email has been registered, please log in'
            }
            else{
                user = userByEmail
                user.merge(data)
                user.token = token
                await user.save()
            }
        }
        else{
            user.fill(data)
            user.token = token
            await user.save()
        }
        
        console.log(user)
        let verificationEmail = Env.get('APP_URL')+'/auth/verification/'+token
        data = user
        data.link = verificationEmail
        MailerService.sendVerificationEmail(user.email, data)
            
        return response.ok({
            id : user.id,
            firstname : user.firstname,
            lastname : user.lastname,
            email : user.email,
            status : user.status
        })
        }catch(err){
            return response.ok(err)
        }
        
    }

    async checkEmailStatus({request, response, params}){
        const id = params.id
        let data = await User.query().where('id', id).hasActive().first()
        if(!data) return response.ok(null, 'account has not been verified', 401)
        const token = await JWTService.generateToken(data)
        return response.ok({access_token:token, access_user:data})
    }

    async resendEmailVerification({request, response}){
        const {email} = request.all()
        const token = UtilityService.generateRandomToken()+new Date().getDate()+new Date().getDay()
        let user = await User.findBy('email', email) 
        if(!user) return response.ok(null, 'Cannot Find This Email', 400)
        user.token = token
        await user.save()
        let verificationEmail = Env.get('APP_URL')+'/auth/verification/'+token
        user.link = verificationEmail
        MailerService.sendVerificationEmail(user.email, user)
        return response.ok(null, 'Verification Email has been sent to your Email')
    }

    async sendEmailForgotPassword({request, response, view}){
        const {email} = request.all()
        const token = UtilityService.generateRandomToken()+new Date().getDate()+new Date().getDay()
        let user = await User.findBy('email', email) 
        if(!user) return response.ok(null, 'Cannot Find this Email', 400)
        user.forgot_token = token
        await user.save()
        let forgotEmail = Env.get('APP_URL')+'/auth/forgotpassword/view/'+token
        user.link = forgotEmail
        MailerService.sendForgotEmail(user.email, user)
        return response.ok(null, 'An Email has been sent to your Email')
    }

    async renderFormForgotPassword({request, response, params, view}){
        const token = params.token
        let user = await User.findBy('forgot_token', token)
        
        if(!user) return view.render('pages.message', {
            data : {
                head : ':(',
                message : 'Sorry',
                sub_message : 'This Url was Expired'
            }
        })
        return view.render('forms.forgot_password', {data:{token}})
    }

    async resetPassword({request, response, params, view, session}){
        const {token, password, co_password} = request.all()
        try{          
            if(password!=co_password) throw 'Password doesn\'t match'
            let user = await User.findBy('forgot_token', token)
            user.password = password
            user.forgot_token = null
            await user.save()
            return view.render('pages.message',{
                data:{
                    head : '\U1F44D',
                    message : 'DONE!!!',
                    sub_message : 'Your Password has been Changed'
                }
            })
        }catch(err){
            session.withErrors({ error: err }).flashAll()
            return response.redirect('back')
        }
        
    }

    async changePassword({request, response}){
        const data = request.all()
        const old_password = data.old_password
        let password = data.new_password
        try{
            let user = await User.query().setHidden(['created_at','updated_at']).where('id', request.decodeToken.id).fetch()
        
            user = user.toJSON()
            user = user[0]

            const isPasswordValid = await Hash.verify(old_password, user.password)
            if(!isPasswordValid) return response.ok(null, 'Wrong Password', 401)
            password = await Hash.make(password)
            await User.query().where('id', request.decodeToken.id).update({password})

            return response.ok('Password has been changed')
        }catch(err){
            return response.ok(err.message)
        }
    }
}

module.exports = AuthController