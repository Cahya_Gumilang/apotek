'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with carts
 */
const Cart = use('App/Models/Cart')
const CartService = use('App/Services/CartService')
const Product = use('App/Models/Product')
class CartController {
  /**
   * Show a list of all carts.
   * GET carts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    try{
    const params = request.all()
    const user_id = request.decodeToken.id
    let query = Cart.query().where('user_id',user_id)
      .with('product', builder=>{
        builder.select(['id','name','thumbnail','price','stok','discount'])
      })
    
      if(params.is_checkout){
        query = query.where('is_checkout', params.is_checkout)
      }
    const data = await query.fetch()
    if(!data.toJSON()[0]) return response.ok(null,"Empty Cart")

    const res = data.toJSON().map(item=>{
      if(item.product.discount>0){
        item.product.price = item.product.price - (item.product.price*item.product.discount/100)
      }
      item.total_price = "Rp"+(item.qty * item.product.price)
      item.product.price = "Rp"+item.product.price
      // item.product.discount_price = item.product.price - (item.product.price*item.product.discount/100)
      item.product.discount = item.product.discount
      return item
    })

    return response.ok(res)
  }catch(err){
    console.log(err.message)
  }
  }

  /**
   * Render a form to be used for creating a new cart.
   * GET carts/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new cart.
   * POST carts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const params = request.all()
    const product_id = params.product_id
    const user_id = request.decodeToken.id
    let qty = params.qty ? params.qty : 1
    qty = Number.parseInt(qty) 
    const res = await Cart.query().where({product_id, user_id}).first()
    console.log(qty)
    if(res) {
      qty += res.qty
      console.log(res.qty)
      await Cart.query().where({product_id, user_id}).update({qty})
    }
    else await Cart.create({product_id, user_id, qty})
    return response.ok(null, 'Dimasukkan ke keranjang')
  }

  /**
   * Display a single cart.
   * GET carts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing cart.
   * GET carts/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update cart details.
   * PUT or PATCH carts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a cart with id.
   * DELETE carts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    let cart = await Cart.find(params.id)
    await cart.delete()
    return response.ok(null, 'Deleted from Cart')
  }
  
  async updateQty ({ params, request, response}) {
    const req = request.all()
  
    const user_id = request.decodeToken.id
   

    const update = await Cart.query().where('user_id', user_id)
      .where('id',params.id)
      .update({qty:req.qty})
      
    const data = await Cart.query()
      .where('user_id',user_id)
      .where('id', params.id)
      .with('product', builder=>{
        builder.select(['id','name','thumbnail','price','stok','discount'])
      })
      .fetch()
    

    const res = data.toJSON().map(item=>{
      if(item.product.discount>0){
        item.product.price = item.product.price - (item.product.price*item.product.discount/100)
      }
      item.total_price = "Rp"+(item.qty * item.product.price)
      item.product.price = "Rp"+item.product.price
      // item.product.discount_price = item.product.price - (item.product.price*item.product.discount/100)
      item.product.discount = item.product.discount
      return item
    })

    return response.ok(res[0], 'Data Updated')
  }

  async setCheckOut ({params, request, response}) {
    const data = request.all()
    let cart = await Cart.find(params.id)
    
    console.log(data.is_checkout)
    cart.is_checkout = data.is_checkout
    await cart.save()
    const res = await CartService.getCheckoutBills(request.decodeToken.id, data.ongkir)
    return response.ok(res)
  }

  async getCheckoutBills ({request, response}) {
    const data = request.all()
    data.ongkir = data.ongkir ? true : false
    const res = await CartService.getCheckoutBills(request.decodeToken.id, data.ongkir)
    console.log(res)
    return response.ok(res)
  }

  async getRelatedProduct ({request, response}) {
    const user_id = request.decodeToken.id
    const data = await Cart.query()
      .where('user_id',user_id)
      .with('product', builder=>{
        builder.select(['id'])
          .with('sub_categories.category')
      }).fetch()
      if(!data.toJSON()[0]) return response.ok(null, 'cart is empty')
    
    let category = []
    let sub_category = []
    for(let item of data.toJSON()){
      for(let item2 of item.product.sub_categories){
        category.push(item2.category.id)
        sub_category.push(item2.id)
      }
    }

    const category_id = Array.from(new Set(category))
    const sub_category_id = Array.from(new Set(sub_category))

      let res = await Product.query()
        .select(['id','name','thumbnail','price','stok','discount'])
        .where('is_deleted', false)
        .whereHas('sub_categories', (builder)=>{
          builder.orWhereIn('product_sub_categories.id', sub_category_id)
            .whereHas('category', (builder2)=>{
              builder2.orWhereIn('product_categories.id', category_id)
            })
        })
        .limit(12)
        .fetch()
        if(!res.toJSON()[0]) return response.ok(null, 'can\'t find any related product' )

        res = res.toJSON().map((item)=>{
          if(item.discount>0){
            item.price = item.price - (item.price*item.discount/100)  
          }
          // item.discount_price = item.price - (item.price*item.discount/100)
          item.discount = item.discount
          return item
        })
        
      return response.ok(res)
  }
}

module.exports = CartController
